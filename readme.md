## Wildfly Spring Boot & JPA Starter

- SpringBootFramwork 1.5.9.RELEASE
- tested with Wildfly-10.1.0.Final
- tested with Postgresql 9.6.6

## How to setup project
```
$ git clone https://gitlab.com/visat09/wildfly-springboot-starter
```

```
$ mvn install
```

## Test
```
http://localhost:8080/AllVerifyService/hello
```

If it work fine.The webpage will show response.
```
[{"user_id":1,"username":"visarut.s","user_fname":"visarut","user_lname":"sae-pueng"}]
```

You can edit context-root in `WEB-INF/jboss-web.xml`
```xml
<?xml version="1.0" encoding="UTF-8"?>
<jboss-web>
    <context-root>/AllVerifyService</context-root>
</jboss-web>
```
