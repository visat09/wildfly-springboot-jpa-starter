package com.wrsp.allverify.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wrsp.allverify.test.ITestService;
import com.wrsp.allverify.test.TestModel;

@RestController
public class SampleHello {
	
	@Autowired
	private ITestService testService;

    @RequestMapping("/hello")
    public ResponseEntity<List<TestModel>> hello() {
    	List<TestModel> list = testService.getAllTest();
    	System.out.println("list >>> " + list);
		return new ResponseEntity<List<TestModel>>(list, HttpStatus.OK);
    }

}
