package com.wrsp.allverify.test;

import java.util.List;

public interface TestDAO {

	List<TestModel> getAllTest();
	
}
