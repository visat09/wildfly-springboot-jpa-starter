package com.wrsp.allverify.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class TestDAOImpl implements TestDAO {
	
	@PersistenceContext	
	private EntityManager entityManager;	

	@SuppressWarnings("unchecked")
	@Override
	public List<TestModel> getAllTest() {
		String hql = "FROM TestModel";
		return (List<TestModel>) entityManager.createQuery(hql).getResultList();
	}

}
