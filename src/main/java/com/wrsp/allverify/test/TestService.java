package com.wrsp.allverify.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService implements ITestService {
	
	@Autowired
	private TestDAO daoTest;

	@Override
	public List<TestModel> getAllTest() {
		System.out.println("[TestService.getAllTest] init");
		return daoTest.getAllTest();
	}

}
