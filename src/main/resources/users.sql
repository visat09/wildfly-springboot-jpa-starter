CREATE TABLE
    users
    (
        user_id INTEGER NOT NULL,
        username CHARACTER VARYING NOT NULL,
        user_fname CHARACTER VARYING,
        user_lname CHARACTER VARYING,
        create_by CHARACTER VARYING,
        create_date TIME(6) WITH TIME ZONE,
        update_by CHARACTER VARYING,
        update_date TIME(6) WITH TIME ZONE,
        PRIMARY KEY (user_id)
    );
    
INSERT INTO users (user_id, username, user_fname, user_lname, create_by, create_date, update_by, update_date) VALUES (1, 'visarut.s', 'visarut', 'sae-pueng', null, null, null, null);